package de.henny022.jini;

/**
 * jini, Created by henny on 08.08.2019.
 */
public class IniValue
{
    private String value;

    public IniValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
