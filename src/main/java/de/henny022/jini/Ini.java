package de.henny022.jini;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * jini, Created by henny on 08.08.2019.
 */
public class Ini
{
    private static Pattern commentLine = Pattern.compile("^[#;].*");
    private static Pattern sectionLine = Pattern.compile("^\\[(.+?)\\]");
    private static Pattern valueLine = Pattern.compile("^(.+?)=(.+?)( [;#].*)?$");

    private Map<String, Map<String, IniValue>> data;

    public static Ini fromFile(String filename)
    {
        Ini ini = new Ini();
        try(Stream<String> lines = Files.lines(Paths.get(filename)))
        {
            AtomicReference<String> section = new AtomicReference<>("");
            lines.forEachOrdered(line -> {
                Matcher matcher = commentLine.matcher(line);
                if(matcher.matches())
                {
                    return;
                }
                matcher = sectionLine.matcher(line);
                if(matcher.matches())
                {
                    section.set(matcher.group(1));
                }
                matcher = valueLine.matcher(line);
                if(matcher.matches())
                {
                    ini.put(section.get(), matcher.group(1), matcher.group(2));
                }
            });
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return ini;
    }

    public void save(String filename) throws IOException
    {
        PrintWriter writer = new PrintWriter(new FileWriter(filename));
        for(Map.Entry<String, Map<String, IniValue>> section : data.entrySet())
        {
            writer.println("[" + section.getKey() + "]");
            for(Map.Entry<String, IniValue> value : data.get(section.getKey()).entrySet())
            {
                writer.println(value.getKey() + "=" + value.getValue().getValue());
            }
        }
        writer.close();
    }

    public Ini()
    {
        data = new HashMap<>();
    }

    public IniValue put(String section, String variable, String value)
    {
        return put(section, variable, new IniValue(value));
    }

    public IniValue put(String section, String variable, IniValue value)
    {
        if(!data.containsKey(section))
        {
            data.put(section, new HashMap<>());
        }
        return data.get(section).put(variable, value);
    }

    public IniValue get(String section, String variable)
    {
        if(!data.containsKey(section))
        {
            return null;
        }
        return data.get(section).get(variable);
    }

}
