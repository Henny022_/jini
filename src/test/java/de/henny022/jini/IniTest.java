package de.henny022.jini;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * jini, Created by henny on 08.08.2019.
 */
public class IniTest
{
    @Test
    public void momo4File() throws IOException
    {
        Ini momo4 = Ini.fromFile("src/test/resources/momo4.ini");
        assertNull(momo4.get("asdf", "asdf"));

        assertEquals("\"1.000000\"", momo4.get("settings", "vsync").getValue());
        assertEquals("\"1.000000\"", momo4.get("settings", "insane dif").getValue());

        assertEquals("\"1.000000\"", momo4.put("settings", "insane dif", "\"0.000000\"").getValue());

        momo4.save("target/momo4.ini");
    }

    @Test
    public void testSave() throws IOException
    {
        Ini ini = new Ini();
        ini.put("asdf", "test", "value");
        ini.save("target/test.ini");
    }
}